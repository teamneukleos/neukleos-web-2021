import { createContext, useContext, useState } from "react";

const MenuContext = createContext({
  isOpen: false,
  setIsOpen: () => {},
});

const MenuProvider = ({ children }) => {
  const [isOpen, setIsOpen] = useState(false);
  return (
    <MenuContext.Provider value={{ isOpen, setIsOpen }}>
      {children}
    </MenuContext.Provider>
  );
};

export default MenuProvider;

export const useMenuContext = () => {
  return useContext(MenuContext);
};
