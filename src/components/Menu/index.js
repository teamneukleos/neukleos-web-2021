import { useEffect, useRef } from "react";
import Image from "next/image";
import Link from "next/link";
import clsx from "clsx";
import { SiBehance, SiDribbble, SiInstagram, SiTwitter } from "react-icons/si";
import { useMenuContext } from ".";

const menuNavLinks = [
  {
    url: "/about-us",
    label: "About Us",
  },
  {
    url: "/portfolio",
    label: "Portfolio",
  },
  {
    url: "/careers",
    label: "Careers",
  },
];

const socialLinks = [
  {
    url: "https://www.instagram.com/teamneukleos/",
    label: "instagram",
    icon: SiInstagram,
  },
  {
    url: "https://www.twitter.com/Neukleos_",
    label: "twitter",
    icon: SiTwitter,
  },
  {
    url: "/",
    label: "behance",
    icon: SiBehance,
  },
  {
    url: "/",
    label: "dribble",
    icon: SiDribbble,
  },
];

const Menu = () => {
  const menuRef = useRef();
  const { isOpen, setIsOpen } = useMenuContext();

  useEffect(() => {
    if (isOpen) {
      menuRef.current.focus();
    }
  }, [isOpen]);

  return (
    <section className={clsx("menu__wrapper", { isOpen })}>
      <div className={clsx("menu__content", { isOpen })}>
        <div className="container menu__content__wrapper">
          <div className="menu__content__left">
            <h2 className="menu__content__title">Like what you see?</h2>
            <form className="contact__form">
              <div className="contact__form__group">
                <label htmlFor="name" hidden />
                <input
                  type="text"
                  id="name"
                  className="contact__form__input"
                  placeholder="Name"
                />
              </div>
              <div className="contact__form__group">
                <label htmlFor="email" hidden />
                <input
                  type="text"
                  id="email"
                  className="contact__form__input"
                  placeholder="Email"
                />
              </div>
              <div className="contact__form__group">
                <label htmlFor="details" hidden />
                <input
                  type="text"
                  id="details"
                  className="contact__form__input"
                  placeholder="Your Enquiry"
                />
              </div>
              <div className="contact__form__group">
                <button type="submit">Send message</button>
              </div>
            </form>
          </div>
          <div className="menu__content__right">
            <nav>
              {/* <div> */}
              <ul className="menu__wrapper__navlinks">
                {menuNavLinks.map(({ url, label }, i) => {
                  return (
                    <li key={`${url}-${i}`}>
                      <Link href={url}>
                        <a
                          ref={i === 0 ? menuRef : null}
                          onClick={() => {
                            setIsOpen(false);
                          }}
                        >
                          {label}
                        </a>
                      </Link>
                    </li>
                  );
                })}
              </ul>
              {/* </div> */}
            </nav>

            <ul className="menu__wrapper__sociallinks">
              {socialLinks.map(({ url, icon, label, icon: Component }, i) => {
                return (
                  <li key={`${url}-${i}`}>
                    <Link href={url} prefetch={false}>
                      <a
                        aria-label={`our ${label} page`}
                        target={"_blank"}
                        rel="noreferrer"
                      >
                        {<Component />}
                      </a>
                    </Link>
                  </li>
                );
              })}
              <li>
                <Link href={"/"}>
                  <a aria-label={`home page`}>Neukleos</a>
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Menu;

export { default as MenuProvider } from "./manager";
export * from "./manager";
