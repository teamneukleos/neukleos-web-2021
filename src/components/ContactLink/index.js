import Link from "next/link";
import { CgArrowLongRight } from "react-icons/cg";

const ContactLink = () => {
  return (
    <section className="contact-link">
      <div className="container contact-link__content">
        <h2 className="contact-link__title">Let&apos;s discuss further</h2>
        <Link href="#contact-us">
          <a className="contact-link__cta">
            Reach out <CgArrowLongRight />
          </a>
        </Link>
      </div>
    </section>
  );
};

export default ContactLink;
