import clsx from "clsx";
import { ImSpinner6 } from "react-icons/im";

const Preloader = ({ show, setShow }) => {
  return (
    <div className={clsx("preloader", { hide: !show })}>
      <div className="container preloader__content">
        <h1>Logo</h1>
        <ImSpinner6 className="preloader__spinner" />
      </div>
    </div>
  );
};

export default Preloader;
