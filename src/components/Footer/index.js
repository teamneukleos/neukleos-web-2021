import Link from "next/link";
import Image from "next/image";
import { SiTwitter, SiBehance, SiDribbble, SiInstagram } from "react-icons/si";

const socialIcons = [
  {
    label: "Instagram",
    url: "https://www.instagram.com/teamneukleos/",
    Icon: SiInstagram,
  },
  {
    label: "Twitter",
    url: "https://www.twitter.com/Neukleos_",
    Icon: SiTwitter,
  },
  { label: "Behance", url: "/", Icon: SiBehance },
  { label: "Dribbble", url: "/", Icon: SiDribbble },
];

const footerNavLinks = [
  { label: "About Us", path: "/about-us" },
  { label: "Portfolio", path: "/portfolio" },
  { label: "Contact Us", path: "#contact-us" },
  { label: "Careers", path: "/careers" },
];

const Footer = () => {
  return (
    <footer className="footer" data-scroll-section>
      <div className="container footer__content">
        <div className="footer__logo">Logo</div>

        {/* Content */}
        <div className="footer__content__wrapper">
          <div className="footer__left">
            <h2 className="footer__title">Want to work with us?</h2>
            <p className="footer__subtitle text-muted">Get In Touch Today!!!</p>
            <form className="contact__form" id="contact-us">
              <div className="contact__form__group">
                <label htmlFor="name" hidden />
                <input
                  type="text"
                  id="name"
                  className="contact__form__input"
                  placeholder="What's your name?"
                />
              </div>
              <div className="contact__form__group">
                <label htmlFor="email" hidden />
                <input
                  type="text"
                  id="email"
                  className="contact__form__input"
                  placeholder="Your Email address"
                />
              </div>
              <div className="contact__form__group">
                <label htmlFor="details" hidden />
                <input
                  type="text"
                  id="details"
                  className="contact__form__input"
                  placeholder="Details about your project"
                />
              </div>
              <div className="contact__form__group">
                <button type="submit">Send Message</button>
              </div>
            </form>
          </div>

          {/* <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              width: "100%",
            }}
          > */}
          <nav className="footer__nav">
            <ul className="footer__right__list footer__navlist">
              {footerNavLinks.map(({ label, path }) => {
                return (
                  <li key={label + path} className="footer__navlink">
                    <Link href={path}>
                      <a>{label}</a>
                    </Link>
                  </li>
                );
              })}
            </ul>
          </nav>

          <div className="footer__sociallinks">
            <ul className="footer__right__list footer__sociallinks__list">
              {socialIcons.map(({ label, url, Icon }) => {
                return (
                  <li key={label} className="footer__social__icons">
                    <Link href={url} prefetch={false}>
                      <a
                        aria-label={`our ${label} page`}
                        target={"_blank"}
                        rel="noreferrer"
                      >
                        <Icon />
                      </a>
                    </Link>
                  </li>
                );
              })}
            </ul>
          </div>
        </div>
        {/* </div> */}

        <div className="footer__bottom text-muted">
          <span>© 2021</span>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
