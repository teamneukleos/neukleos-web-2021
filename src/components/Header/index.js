import Link from "next/link";
import clsx from "clsx";
import { useRouter } from "next/router";
import { useMenuContext } from "@components/Menu";

const isWhiteList = ["/", "/portfolio", "/about-us"];

const Header = () => {
  const { isOpen, setIsOpen } = useMenuContext();
  const router = useRouter();

  const isWhite = isWhiteList.includes(router.asPath);

  return (
    <header className={clsx("header", { isOpen })} data-scroll-section>
      <div
        className="container header__content"
        data-is-text-white={isWhite}
        // data-is-nav-open={isOpen}
      >
        {/* Logo */}
        <div>
          <Link href="/">
            <a
              aria-label="logo"
              className="logo"
              data-is-text-white={isWhite}
              data-is-nav-open={isOpen}
            >
              Logo
            </a>
          </Link>
        </div>

        <div>
          {/* CTA */}
          <Link href="#contact-us">
            <a className="header__cta">Get In Touch</a>
          </Link>

          {/* Hamburger */}
          <button
            aria-label={`${isOpen ? "Close" : "Open"} hamburger menu`}
            className={clsx("hamburger-menu", { isOpen })}
            onClick={() => setIsOpen(!isOpen)}
          >
            <span />
            <span />
            <span />
          </button>
        </div>
      </div>
    </header>
  );
};

export default Header;
