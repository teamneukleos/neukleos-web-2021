import Footer from "@components/Footer";
import Header from "@components/Header";
import Menu, { MenuProvider } from "@components/Menu";
import Preloader from "@components/Preloader";
import { useEffect, useState } from "react";
// import { useRouter } from "next/router";
// import { useRef } from "react";
// import { LocomotiveScrollProvider } from "react-locomotive-scroll";

const PageLayout = ({ children }) => {
  const [showPreloader, setShowPreloader] = useState(false);
  // const containerRef = useRef(null);
  // const { asPath } = useRouter();

  useEffect(() => {
    let isMounted = true;

    // if sessionstorage return
    const isVisited = !!sessionStorage.getItem("neuk-visited");

    if (isVisited) {
      if (isMounted) {
        setShowPreloader(false);
        return;
      }
    } else {
      setShowPreloader(true);
    }

    const preloaderTimeOut = setTimeout(() => {
      sessionStorage.setItem("neuk-visited", true);
      if (isMounted) {
        setShowPreloader(false);
      }
    }, 2000);

    return () => {
      isMounted = false;
      clearTimeout(preloaderTimeOut);
    };
  }, []);

  return (
    <>
      <Preloader show={showPreloader} setShow={setShowPreloader} />

      {!showPreloader && (
        <div
          id="page-wrapper"
          data-scroll-container
          // ref={containerRef}
          style={{ overflow: "hidden" }}
        >
          <MenuProvider>
            <Menu />
            <Header />
          </MenuProvider>
          {children}
          <Footer />
        </div>
      )}
    </>
  );
};

export default PageLayout;
