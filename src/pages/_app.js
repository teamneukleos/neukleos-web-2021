import { IdProvider } from "@radix-ui/react-id";
import "@styles/globals.scss";

function MyApp({ Component, pageProps }) {
  return (
    <IdProvider>
      <Component {...pageProps} />
    </IdProvider>
  );
}

export default MyApp;
