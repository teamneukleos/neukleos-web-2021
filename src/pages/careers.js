import Head from "next/head";
import { PageLayout } from "@components/Layout";
import { Hero } from "@containers/Careers";
import Opportunities from "@containers/Careers/Opportunities";
import { jobs } from "@lib/data/careers";

export default function Portfolio({ jobs = [] }) {
  return (
    <PageLayout>
      <Head>
        <title>Neukleos | Careers</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className="careers" data-scroll-section>
        <Hero />
        <Opportunities jobs={jobs} />
      </main>
    </PageLayout>
  );
}

export async function getStaticProps(context) {
  return {
    props: { jobs },
    // revalidate: 10,
  };
}
