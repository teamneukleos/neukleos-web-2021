import { PageLayout } from "@components/Layout";
import Head from "next/head";

export default function Custom404() {
  return (
    <PageLayout>
      <Head>
        <title>Neukleos | Page Not Found</title>
      </Head>
      <main className="error error-404" data-scroll-section>
        <section className="hero">
          <div className="container hero__content">
            <div className="careers__container hero__content__text">
              <h1 className="hero__title">404 - Page Not Found</h1>
              <p className="hero__subtitle text-muted">
                Unfortunatly the page you&apos;re looking for cannot be found.
              </p>
            </div>
          </div>
        </section>
      </main>
    </PageLayout>
  );
}
