import { works } from "@lib/data/works";

// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

export default function handler(req, res) {
  const recentWorks = works
    .sort((a, b) => b.createdOn - a.createdOn)
    .slice(0, 6);

  res.status(200).json(recentWorks);
}
