import { works } from "@lib/data/works";

export default function caseStudyHandler({ query: { slug } }, res) {
  const filtered = works.filter((caseStudy) => caseStudy.slug === slug);

  if (filtered.length > 0) {
    res.status(200).json(filtered[0]);
  } else {
    res.status(404).json(JSON.stringify(null));
  }
}
