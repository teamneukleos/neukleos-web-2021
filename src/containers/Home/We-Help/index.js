import Image from "next/image";
import Link from "next/link";

const WeHelp = () => {
  return (
    <section className="we-help">
      <div className="container we-help__content">
        <div className="we-help__content__wrapper">
          <div className="we-help__content__text">
            <h2 className="we-help__title">
              We help CMOs & CXOs do more together.
            </h2>
            <p className="we-help__description">
              We understand how brand informs experience, and how behaviors
              guide marketing; all the way from culture, to conversion. We
              design and build the digital services, new routes to commerce and
              marketing campaigns that deliver on a company’s values, every day.
            </p>
            <Link href="/">
              <a className="we-help__cta">Learn more</a>
            </Link>
          </div>
        </div>

        <div className='we-help__image__wrapper'>
          <div className="we-help__image we-help__image-1">
            <div className="we-help__image__content">
              <Image
                src="/images/we-help-chart.jpg"
                alt="we help banner"
                layout="fill"
                placeholder="blur"
                blurDataURL="/images/we-help-chart.jpg"
              />
            </div>
          </div>
          <div className="we-help__image we-help__image-2">
            <div className="we-help__image__content">
              <Image
                src="/images/we-help-union.jpg"
                alt="we help banner union bank"
                layout="fill"
                placeholder="blur"
                blurDataURL="/images/we-help-union.jpg"
              />
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default WeHelp;
