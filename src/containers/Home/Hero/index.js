import Link from "next/link";
import { useCallback } from "react";
import { CgChevronDown } from "react-icons/cg";

const Hero = () => {
  const scrollToServices = useCallback(() => {
    const section = document.querySelector("#services");
    section.scrollIntoView({ behavior: "smooth", block: "start" });
  }, []);

  return (
    <section className="hero">
      <div className="hero__bg">
        <video className="hero__bg-video" autoPlay loop muted>
          <source src="/videos/hero-video.mp4#t=126,140" type="video/mp4" />
        </video>
      </div>
      <div className="container hero__content">
        <div className="hero__content__text">
          <h1 className="hero__title">Less is more</h1>
          <p className="hero__subtitle">
            We are the digital agency of the future, focusing on developing
            strategy and content that build brands and creating meaningful
            digital experiences.
          </p>
        </div>

        <span className="hero__learn-more" onClick={scrollToServices}>
          Learn more
          <CgChevronDown />
        </span>
      </div>
    </section>
  );
};

export default Hero;
