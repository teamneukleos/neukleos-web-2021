const services = [
  {
    title: "Branding",
    scope: [
      "Brand Strategy & Positioning",
      "Visual Identity",
      "Brand Architecture",
      "Naming & Verbal Identity",
      "Brand Guidelines",
    ],
  },
  {
    title: "Product Design",
    scope: [
      "User Interface & Experience",
      "Design Systems & Style Guides",
      "Web Development",
      "Naming & Verbal Identity",
      "Mobile Applications",
    ],
  },
  {
    title: "Brand Engagement",
    scope: [
      "Campaign & Ad Development",
      "Publications & Editorial Design",
      "Event & Environmental Branding",
      "Social Media Communication",
      "Design Production & Execution",
      "Brand Management (Retainer)",
    ],
  },
  {
    title: "Content Development",
    scope: [
      "Content Strategy",
      "Data Visualization",
      "Creative Direction",
      "Motion & Graphic Design",
      "Copy Writing",
    ],
  },
];

const Services = () => {
  return (
    <section className="services" id="services">
      <div className="container services__content">
        <div className="services__content__wrapper">
          <div className="services__content__text">
            <h2 className="services__title">What we do</h2>
            <p className="services__description text-muted--light">
              Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
              nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
              erat, sed diam voluptua. At vero eos et accusam et justo duo
              dolores et ea rebum. Stet clita kasd gubergren, no sea takimata.
            </p>
          </div>

          <div className="services__card__list">
            {services.map(({ title, scope }) => {
              return (
                <div key={title} className="services__card">
                  <div className="services__card__content">
                    <h3 className="services__card__title text-muted--light">
                      {title}
                    </h3>
                    <ul className="services__card__scope">
                      {scope.map((item) => {
                        return (
                          <li
                            key={item}
                            className="services__card__scope__item"
                          >
                            {item}
                          </li>
                        );
                      })}
                    </ul>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </section>
  );
};

export default Services;
