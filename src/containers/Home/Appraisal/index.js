import Image from "next/image";

const Appraisal = () => {
  return (
    <section className="appraisal">
      <div className="container appraisal__content">
        <div className="appraisal__content__wrapper">
          <div className="appraisal__content__image__wrapper">
            <div className="appraisal__content__image">
              <Image
                src="/images/character.png"
                alt="neukleos character"
                layout="fill"
              />
            </div>
          </div>
          <div className="appraisal__content__text">
            <h2 className="appraisal__title">Data is the new oil</h2>
            <p className="appraisal__description text-muted--light">
              We are the digital agency of the future, focusing on developing
              strategy and content that build brands and creating meaningful
              digital experiences.
            </p>
            <button className="btn primary-btn appraisal__cta">
              Get an appraisal
            </button>
          </div>
        </div>
        <div className="appraisal__dot__image__wrapper">
          <div className="appraisal__dot__image">
            <Image src="/images/appraisal.svg" alt="appraisal" layout="fill" />
          </div>
        </div>
      </div>
    </section>
  );
};

export default Appraisal;
