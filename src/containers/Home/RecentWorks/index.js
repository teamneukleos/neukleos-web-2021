import Image from "next/image";
import Link from "next/link";

const recentWorks = [
  {
    slug: "union-bank",
    image: "/images/rise-union_bank.png",
    // image: "/images/ghana-club_beer.png",
    title: "Union Bank",
    subtitle: "Rise challenge",
  },
  {
    slug: "club-premium-lager",
    image: "/images/ghana-club_beer.png",
    title: "Club Beer",
    subtitle: "Rise challenge",
  },
  {
    slug: "lucozade-boost",
    image: "/images/boost-lucozade2.png",
    title: "Lucozade Boost",
    subtitle: "Rise challenge",
  },
  {
    slug: "tlcc",
    image: "/images/boost-lucozade.png",
    title: "Boost Challenge",
    subtitle: "Rise challenge",
  },
  {
    slug: "lucozade-boost-1",
    image: "/images/boost-lucozade2.png",
    title: "Lucozade Boost",
    subtitle: "Rise challenge",
  },
  {
    slug: "tlcc-4",
    image: "/images/boost-lucozade.png",
    title: "Boost Challenge",
    subtitle: "Rise challenge",
  },
];

const RecentWorks = ({ data = [] }) => {
  return (
    <section className="recent__works">
      <div className="container recent__works__content">
        <h2 className="recent__works__content__title">Recent Works</h2>

        <div className="recent__works__content__list">
          {data.map(({ title, banner, slug, content }) => {
            const displayImage =
              banner ?? content[0].images[0] ?? content[1].images[0] ?? null;

            return (
              <Link key={slug} href={`/case-study/${slug}`}>
                <a className="recent__works__card__wrapper">
                  <div className="recent__works__card">
                    <div className="recent__works__card__image">
                      {displayImage && (
                        <Image
                          src={displayImage}
                          alt={title}
                          layout="fill"
                          placeholder="blur"
                          blurDataURL={displayImage}
                        />
                      )}
                    </div>
                    {/* <div className="recent__works__card__content">
                      <h4 className="recent__works__card__title">{title}</h4>
                      <p className="recent__works__card__subtitle">
                        {subtitle}
                      </p>
                    </div> */}
                  </div>
                </a>
              </Link>
            );
          })}
        </div>
      </div>
    </section>
  );
};

export default RecentWorks;
