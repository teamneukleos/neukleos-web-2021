import Image from "next/image";

const clients = [
  {
    label: "Union Bank",
    image: "/images/clients/ubn.png",
    type: "square",
  },
  {
    label: "Pernod Ricard",
    image: "/images/clients/pernod-ricard.png",
  },
  {
    label: "Heineken",
    image: "/images/clients/heineken.png",
  },
  {
    label: "Samsung",
    image: "/images/clients/samsung.png",
  },
  {
    label: "edu360",
    image: "/images/clients/edu360.png",
  },
  {
    label: "Coca-Cola",
    image: "/images/clients/coca-cola.png",
  },
  {
    label: "Moet & Chandon",
    image: "/images/clients/moet-chandon.png",
  },
  {
    label: "Art X",
    image: "/images/clients/art-x.png",
    type: "square",
  },
  {
    label: "Fanta",
    image: "/images/clients/fanta.jpg",
    type: "square",
  },

  {
    label: "ARM Pension",
    image: "/images/clients/arm-pension.png",
  },
  {
    label: "Gulder",
    image: "/images/clients/gulder.png",
    type: "square",
  },
  {
    label: "Lucozade",
    image: "/images/clients/lucozade.jpg",
  },
];

const Clients = () => {
  return (
    <section className="clients">
      <div className="container clients__content">
        <div className="clients__content__wrapper">
          <div className="clients__content__text">
            <h2 className="clients__title">Our Clients</h2>
            <p className="clients__description text-muted--light">
              Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
              nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
              erat, sed diam voluptua. At vero eos et accusam et justo duo
              dolores et ea rebum. Stet clita kasd gubergren, no sea takimata.
            </p>
          </div>

          <div className="clients__card__list">
            {clients.map(({ label, image, type }) => {
              return (
                <div
                  key={label}
                  className="clients__card"
                  data-aspect-type={type}
                >
                  <Image src={image} alt={label} layout="fill" />
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </section>
  );
};

export default Clients;
