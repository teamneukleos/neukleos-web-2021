import Link from "next/link";
import { CgChevronDown } from "react-icons/cg";

const Hero = () => {
  return (
    <section className="hero">
      <div className="container hero__content">
        <div className="careers__container hero__content__text">
          <h1 className="hero__title">What drives you?</h1>
          <p className="hero__subtitle text-muted">
            If you got energy, enthusiasm and creativity, we want you
          </p>
        </div>
      </div>
    </section>
  );
};

export default Hero;
