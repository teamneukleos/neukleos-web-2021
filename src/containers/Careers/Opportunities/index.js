import Link from "next/link";
import { CgArrowDown } from "react-icons/cg";
import * as Collapsible from "@radix-ui/react-collapsible";

const samplejobs = [
  {
    id: "remote-1",
    location: "Remote",
    experience: "5+ years",
    skills: ["Remote"],
    title: "Front-end Developer (Remote)",
    description:
      "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et.",
  },
];

const JobCard = ({ data }) => {
  const { title, location, experience, skills, description } = data;
  return (
    <>
      <Collapsible.Trigger className="opportunities__trigger">
        <span className="opportunities__trigger__title">{title}</span>
        <span className="opportunities__trigger__icon">
          <CgArrowDown />
        </span>
      </Collapsible.Trigger>
      <Collapsible.Content className="opportunities__content__wrapper">
        <div className="opportunities__content">
          <div className="opportunities__content__info__wrapper">
            {/* Location */}
            <div className="opportunities__content__info">
              <span className="opportunities__content__info__title">
                Location
              </span>
              <span className="opportunities__content__info__description">
                {location}
              </span>
            </div>

            {/* Experience */}
            <div className="opportunities__content__info">
              <span className="opportunities__content__info__title">
                Experience
              </span>
              <span className="opportunities__content__info__description">
                {experience}
              </span>
            </div>

            {/* Required Skills */}
            <div className="opportunities__content__info">
              <span className="opportunities__content__info__title">
                Required Skills
              </span>
              <span className="opportunities__content__info__description">
                {skills.map((item) => {
                  return <span key={item}>{item}</span>;
                })}
              </span>
            </div>
          </div>
          <div className="opportunities__content__description__wrapper">
            <p className="opportunities__content__description">{description}</p>
            <button className="opportunities__content__cta">Apply</button>
          </div>
        </div>
      </Collapsible.Content>
    </>
  );
};

const Opportunities = ({ jobs = samplejobs }) => {
  return (
    <section className="opportunities">
      <div className="container opportunities__content">
        <div className="careers__container opportunities__content__text">
          <h2 className="opportunities__title">Current Opportunities</h2>

          <Collapsible.Root>
            <ul className="opportunities__list">
              {jobs.map((job) => {
                return (
                  <li key={job.id}>
                    <JobCard data={job} />
                  </li>
                );
              })}
            </ul>
          </Collapsible.Root>

          {jobs.length < 1 && <p>No open opportunities at the moment!</p>}
        </div>
      </div>
    </section>
  );
};

export default Opportunities;
