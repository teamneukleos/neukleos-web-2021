import Image from "next/image";

const MainContent = ({ data }) => {
  const { content = [], result = [] } = data;

  return (
    <>
      {(content.length > 0 || result.length > 0) && (
        <section className="main-content">
          <div className="container main-content__content">
            <div className="main-content__content__group">
              {content.map(({ title, description, images }, index) => {
                return (
                  <div
                    key={title + index}
                    className="main-content__content__block main-content__content__block--content"
                  >
                    <div className="main-content__content__text">
                      <h2 className="main-content__content__title">{title}</h2>
                      <p className="main-content__content__description">
                        {description}
                      </p>
                    </div>

                    <div className="main-content__content__image-list">
                      {images.map((item, index) => {
                        return (
                          <div
                            key={item + index}
                            className="main-content__content__image__wrapper"
                            data-index={index + 1}
                          >
                            <div
                              key={item + index}
                              className="main-content__content__image"
                            >
                              <Image src={item} alt={title} layout="fill" />
                            </div>
                          </div>
                        );
                      })}
                    </div>
                  </div>
                );
              })}
            </div>

            {result.length > 0 && (
              <div className="main-content__content__group">
                <span>
                  <h2 className="main-content__content__title">Results</h2>
                </span>

                {result.map(({ title, description, image, units }, index) => {
                  return (
                    <div
                      key={title + index}
                      className="main-content__content__block main-content__content__block--result"
                    >
                      <div className="main-content__content__text">
                        <h3 className="main-content__content__title">
                          {title}
                          {units && (
                            <span className="main-content__content__title--units">
                              {units}
                            </span>
                          )}
                        </h3>
                        <p className="main-content__content__description">
                          {description}
                        </p>
                      </div>

                      {image && (
                        <div className="main-content__content__image-list">
                          <div
                            key={image + index}
                            className="main-content__content__image__wrapper"
                            data-index={index + 1}
                          >
                            <div
                              key={image + index}
                              className="main-content__content__image"
                            >
                              <Image src={image} alt={title} layout="fill" />
                            </div>
                          </div>
                        </div>
                      )}
                    </div>
                  );
                })}
              </div>
            )}
          </div>
        </section>
      )}
    </>
  );
};

export default MainContent;
