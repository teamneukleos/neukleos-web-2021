import Image from "next/image";

const Hero = ({ title, subtitle, description, banner }) => {
  return (
    <section className="hero">
      <div className=" hero__content">
        <div className="hero__banner">
          {banner && (
            <Image src={banner} alt={title + " banner"} layout="fill" />
          )}
        </div>
        <div className="container hero__content__top">
          <div className="hero__content__text">
            <h1 className="hero__title">{title}</h1>
            <h1 className="hero__subtitle">{subtitle}</h1>
            <p className="hero__description text-muted--light">{description}</p>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Hero;
