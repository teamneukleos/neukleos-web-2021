import Link from "next/link";
import Image from "next/image";
import { CgArrowLongRight } from "react-icons/cg";
import { RiArrowDropDownFill } from "react-icons/ri";
import * as DropdownMenu from "@radix-ui/react-dropdown-menu";
import { useMemo, useState } from "react";

const sortOptions = {
  desc: "Newest first",
  asc: "Oldest first",
};

// const filterOptions = {
//   industries: { label: "industries", value: ["Banking", "Food", "Technology"] },
//   clients: { label: "clients", value: ["Union Bank", "Pernod Ricard"] },
// };

const Works = ({ data }) => {
  const [filter, setFilter] = useState({
    industries: [],
    clients: [],
  });
  const [sort, setSort] = useState("desc");

  const filterOptions = useMemo(() => {
    const toFilter = {
      industries: {
        label: "industries",
        value: [...new Set(data.map(({ industries }) => industries))],
      },
      clients: {
        label: "clients",
        value: [...new Set(data.map(({ clients }) => clients))],
      },
    };

    return toFilter;
  }, [data]);

  const filteredData = useMemo(() => {
    if (filter.clients.length < 0 && filter.industries.length < 0) {
      return data;
    }

    const sortFun = {
      asc: (a, b) => {
        return a.createdOn - b.createdOn;
      },
      desc: (a, b) => {
        return b.createdOn - a.createdOn;
      },
    };

    const filterValue = (arr = [], criteria) => {
      return arr.filter((item) => {
        const itemCrit = item[criteria].toLocaleString();
        return filter[criteria].includes(itemCrit);
      });
    };

    let filtered = data;
    Object.entries(filter).map(([key, val]) => {
      if (val.length > 0) {
        filtered = filterValue(filtered, key);
      }
      return filtered;
    });

    const sorted = filtered.sort(sortFun[sort]);

    return sorted;
  }, [data, filter, sort]);

  return (
    <section className="works">
      <div className="container works__content">
        <div className="works__content__main">
          <div className="works__content__controls">
            <div className="works__content__controls__sort">
              <span>Filter by</span>

              <DropdownMenu.Root className="works__dropdown">
                <DropdownMenu.Trigger className="works__dropdown__trigger works__content__controls__sort__trigger">
                  {sortOptions[sort]} <RiArrowDropDownFill />
                </DropdownMenu.Trigger>
                <DropdownMenu.Content className="works__dropdown__content">
                  {Object.entries(sortOptions).map(([key, val]) => {
                    return (
                      <DropdownMenu.Item
                        key={key}
                        className="works__dropdown__item"
                        onClick={() => setSort(key)}
                      >
                        {val}
                      </DropdownMenu.Item>
                    );
                  })}
                </DropdownMenu.Content>
              </DropdownMenu.Root>
            </div>

            <div className="works__content__controls__filters">
              {Object.entries(filterOptions).map(([key, val]) => {
                return (
                  <DropdownMenu.Root key={key} className="works__dropdown">
                    <DropdownMenu.Trigger className="works__dropdown__trigger">
                      {val.label} <RiArrowDropDownFill />
                    </DropdownMenu.Trigger>
                    <DropdownMenu.Content
                      side="right"
                      align="start"
                      className="works__dropdown__content"
                    >
                      {val.value.map((item, index) => {
                        const isSelected = filter[key].includes(item);

                        return (
                          <DropdownMenu.CheckboxItem
                            key={item + index}
                            className="works__dropdown__item"
                            checked={isSelected}
                            onCheckedChange={(e) => {
                              if (isSelected) {
                                setFilter((prev) => {
                                  const newFilter = [...prev[key]].filter(
                                    (it) => {
                                      return it !== item;
                                    }
                                  );

                                  return {
                                    ...prev,
                                    [key]: newFilter,
                                  };
                                });
                              } else {
                                setFilter((prev) => {
                                  const newFilter = new Set([
                                    ...prev[key],
                                    item,
                                  ]);
                                  return {
                                    ...prev,
                                    [key]: [...newFilter],
                                  };
                                });
                              }
                            }}
                          >
                            <DropdownMenu.ItemIndicator className="works__dropdown__itemIndicator">
                              <input type="checkbox" checked={isSelected} />
                            </DropdownMenu.ItemIndicator>
                            {item}
                          </DropdownMenu.CheckboxItem>
                        );
                      })}
                    </DropdownMenu.Content>
                  </DropdownMenu.Root>
                );
              })}
            </div>
          </div>
          <div className="works__content__list">
            {filteredData.map((work) => {
              const displayImage =
                work.banner ??
                work.content[0].images[0] ??
                work.content[1].images[0] ??
                null;

              return (
                <Link key={work.slug} href={`/case-study/${work.slug}`}>
                  <a>
                    <div className="works__card">
                      <div className="works__card__image">
                        {displayImage && (
                          <Image
                            src={displayImage}
                            alt={work.title}
                            layout="fill"
                            placeholder="blur"
                            blurDataURL={displayImage}
                            // priority
                          />
                        )}
                      </div>
                      <div className="works__card__content">
                        <p className="works__card__title">{work.clients}</p>
                        <p className="works__card__subtitle text-muted--light">
                          {work.title}
                        </p>
                        <span className="works__card__cta">
                          View project <CgArrowLongRight />
                        </span>
                      </div>
                    </div>
                  </a>
                </Link>
              );
            })}
          </div>
        </div>
      </div>
    </section>
  );
};

export default Works;
