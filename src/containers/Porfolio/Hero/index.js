import Link from "next/link";
import { CgChevronDown } from "react-icons/cg";

const Hero = () => {
  return (
    <section className="hero">
      <div className="hero__bg">
        <video className="hero__bg-video" autoPlay loop muted>
          <source
            src="/videos/hero-video.mp4#t=77,94"
            type="video/mp4"
          />
        </video>
      </div>
      <div className="container hero__content">
        <div className="hero__content__text hero__heading__sub">
          <h2 className="hero__title">Together, we achieve greater</h2>
          <p className="hero__subtitle">
            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
            nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
            erat, sed diam voluptua. At vero eos et accusam et justo duo dolores
            et ea rebum. Stet clita.
          </p>
        </div>

        <div className="hero__content__text  hero__heading__main">
          <h1 className="hero__title">Our Work</h1>
        </div>
      </div>
    </section>
  );
};

export default Hero;
