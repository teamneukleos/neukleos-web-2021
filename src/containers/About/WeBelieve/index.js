import Image from "next/image";

const displayImages = [
  "/images/fabien-pexels.jpg",
  "/images/fabien-pexels.jpg",
  "/images/experience-design-pexel.jpg",
  "/images/computer-pexels.jpg",
];

const WeBelieve = () => {
  return (
    <section className="we-believe">
      <div className="container we-believe__content">
        <div className="we-believe__content__text">
          <h2 className="we-believe__title">Digital is not a department</h2>
          <p className="we-believe__subtitle text-muted--light">
            We strongly believe that digital is not a department within
            marketing but that real digital transformation must permeate all
            facets of any organization; policy, process, product with C-Level
            ownership.
          </p>
        </div>
        <div className="we-believe__image__list">
          {displayImages.map((item, index) => {
            return (
              <div
                key={item + index}
                className={`we-believe__image__wrapper we-believe__image__wrapper-${
                  index + 1
                }`}
              >
                <div className="we-believe__image">
                  <Image src={item} alt="We believe" layout="fill" />
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </section>
  );
};

export default WeBelieve;
