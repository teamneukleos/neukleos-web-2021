export { default as Hero } from "./Hero";
export { default as WeBelieve } from "./WeBelieve";
export { default as Team } from "./Team";
export { default as Awards } from "./Awards";
