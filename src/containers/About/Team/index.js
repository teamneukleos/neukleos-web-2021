import Image from "next/image";

const team = [
  {
    fullname: "Bukola Akingbade",
    position: "CEO, Managing Partner",
    image: "/images/people/GB.jpg",
  },
  {
    fullname: "Obiora Ede",
    position: "COO, Managing Partner",
    image: "/images/people/Tosin.jpg",
  },
  {
    fullname: "Timothy Temi",
    position: "COO, Managing Partner",
    image: "/images/people/Michael.jpg",
  },
  {
    fullname: "Adeboye Timi",
    position: "COO, Managing Partner",
    image: "/images/people/Temitayo.jpg",
  },
  {
    fullname: "Bukola Akingbade",
    position: "CEO, Managing Partner",
    image: "/images/people/GB.jpg",
  },
  {
    fullname: "Obiora Ede",
    position: "COO, Managing Partner",
    image: "/images/people/Tosin.jpg",
  },
  {
    fullname: "Timothy Temi",
    position: "COO, Managing Partner",
    image: "/images/people/Michael.jpg",
  },
  {
    fullname: "Adeboye Timi",
    position: "COO, Managing Partner",
    image: "/images/people/Temitayo.jpg",
  },
];

const Team = () => {
  return (
    <section className="team">
      <div className="container team__content">
        <h2 className="team__title">Our team</h2>
        <p className="team__subtitle text-muted--light">
          Starting with a founding balance of innovation and management, we have
          grown into a beautiful mix of artists, storytellers, managers,
          innovators, builders, optimists and optimistic realists, with skill
          sets across graphic design, animation, UI/UX, copywriting, strategy
          development and more.
        </p>

        <div className="team__list">
          {team.map(({ fullname, position, image }, index) => {
            return (
              <div key={fullname + index} className="team__card">
                <div className="team__card__top">
                  <div className="team__card__image">
                    <Image
                      src={image}
                      alt={fullname}
                      layout="fill"
                      placeholder="blur"
                      blurDataURL={image}
                    />
                  </div>
                </div>
                <div className="team__card__details">
                  <p className="team__card__fullname">{fullname}</p>
                  <p className="team__card__position">{position}</p>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </section>
  );
};

export default Team;
