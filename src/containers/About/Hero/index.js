const Hero = () => {
  return (
    <section className="hero">
      <div className="container hero__content">
        <div className="hero__content__text">
          <h1 className="hero__title">
            <span className="hero__title-mute hero__title-mute--1">
              Culture
            </span>
            <span>+</span>
            <span className="hero__title-mute hero__title-mute--2">Design</span>
          </h1>
          <p className="hero__subtitle text-muted--light">
            Neukleos is a full service digital agency. As the digital agency of
            the future, Neukleos focuses on developing strategy and content that
            build brands and creating meaningful digital experiences.
          </p>
        </div>
      </div>
    </section>
  );
};

export default Hero;
