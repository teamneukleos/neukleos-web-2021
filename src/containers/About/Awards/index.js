import Image from "next/image";

const awards = [
  {
    label: "Awwwards",
    type: "square",
    image: "/images/awards/awwwards.png",
  },
  {
    label: "Marketing World Awards 2021",
    // type: "square",
    image: "/images/awards/mwa21.png",
  },
  {
    label: "Awwwards",
    type: "square",
    image: "/images/awards/awwwards.png",
  },
  {
    label: "Marketing World Awards 2021",
    // type: "square",
    image: "/images/awards/mwa21.png",
  },
  {
    label: "Awwwards",
    type: "square",
    image: "/images/awards/awwwards.png",
  },
  {
    label: "Marketing World Awards 2021",
    // type: "square",
    image: "/images/awards/mwa21.png",
  },
];

const Awards = () => {
  return (
    <section className="awards">
      <div className="container awards__content">
        <h2 className="awards__title">Our Awards</h2>
        <p className="awards__subtitle">
          Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
          nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat,
          sed diam voluptua. At vero eos et accusam et justo duo dolores et.
        </p>

        <div className="awards__list">
          {awards.map(({ label, image, type = "" }, index) => {
            return (
              <div
                key={label + index}
                className="awards__image"
                data-orientation={type}
              >
                <Image src={image} alt={label} layout="fill" />
              </div>
            );
          })}
        </div>
      </div>
    </section>
  );
};

export default Awards;
