export const works = [
  {
    createdOn: new Date(2021, 7, 8).toLocaleString(),
    slug: "uncle-thomas",
    color: "#00AEED",
    title: "Uncle Thomas",
    clients: "Union Bank",
    industries: "Banking",
    description: "the simpler, smarter bank",
    banner: "/images/case-study/union-bank/uncle-thomas-2.jpg",
    content: [
      {
        title: "The Brief",
        description: "Change Brand Perception",
        images: [],
      },
    ],
    result: [
      {
        title: "15000",
        units: "signups",
        description:
          "Optimizing UAC for Signup vs. App Downloads. Optimizing Cost-Per-Signup. Campaign Paused to due Challenges with the App.",
        image: "/images/case-study/union-bank/uncle-thomas-5.jpg",
      },
      {
        title: "5MILLION",
        units: "reach",
        description:
          "Optimizing UAC for Signup vs. App Downloads. Optimizing Cost-Per-Signup",
        image: "/images/case-study/union-bank/uncle-thomas-5.jpg",
      },
    ],
  },
  {
    createdOn: new Date(2021, 7, 8).toLocaleString(),
    // color: "#00AEED",
    slug: "arm-payday-investor",
    title: "ARM Payday Investor",
    clients: "ARM Payday Investor",
    industries: "Banking",
    description: "The smart way to invest",
    banner: null,
    content: [
      {
        title: "The Brief",
        description:
          "Develop a digital roadmap for ARM channel migration and PayDay Investor app download.",
        images: [
          "/images/case-study/arm-payday-investor/arm-payday-investor-1.jpg",
          "/images/case-study/arm-payday-investor/arm-payday-investor-2.jpg",
        ],
      },
    ],
    result: [
      {
        title: "FIREBASE",
        units: null,
        description:
          "Over 10,000 Signups. Optimizing UAC for Signup vs. App Downloads. Optimizing Cost-Per-Signup. Campaign Paused & Relaunched 2019",
        image:
          "/images/case-study/arm-payday-investor/arm-payday-investor-4.png",
      },
    ],
  },
  {
    createdOn: new Date(2021, 7, 8).toLocaleString(),
    // color: "#00AEED",
    slug: "hennessy-artistry",
    title: "Hennessy Artistry",
    clients: "Moet Hennessy",
    description: "All I Need",
    industries: "Drinks",
    banner: "/images/case-study/moet-hennessy/hennessy-artistry-1.jpg",
    content: [
      {
        title: "The Brief",
        description:
          "Execute the Hennessy Artistry digital campaign 2012, 2013, 2014, 2015, 2016, 2017 & 2018 – Hennessy VS Class, Jonathan Mannion, Hennessy Cypher, VSOP 200th Anniversary",
        images: [],
      },
    ],
    result: [
      {
        title: "FIREBASE",
        units: null,
        description:
          "Creative Storytelling to drive Customer Acquisition. Optimizing Conversions; Funnels, Events & Audiences. Over 10,000 Signups. Optimizing UAC for Signup vs. App Downloads. Optimizing Cost-Per-Signup. Campaign Paused & Relaunched 2019",
        image: "/images/case-study/moet-hennessy/hennessy-artistry-1.jpg",
      },
    ],
  },
  {
    createdOn: new Date(2021, 7, 8).toLocaleString(),
    // color: "#00AEED",
    slug: "la-casera",
    title: "La Casera",
    clients: "The La Casera Company",
    industries: "Drinks",
    description: "If there is no Casera we will leave",
    banner: "/images/case-study/tlcc/lacasera-1.png",
    content: [
      {
        title: "The Solution",
        description:
          "Using spoken words Artist influencers to push the message on digital- Huwa, Bunmi Africa, and others to push the poem and words on digital. For the judge, we used Wanawana as the main judge to also influence people to participate in.",
        images: [],
      },
    ],
    result: [],
  },
  {
    createdOn: new Date(2021, 7, 8).toLocaleString(),
    // color: "#00AEED",
    slug: "smoov",
    title: "Smoov",
    clients: "The La Casera Company",
    industries: "Drinks",
    description: "Fun is an attitude",
    banner: "/images/case-study/tlcc/smoov-1.jpg",
    content: [
      {
        title: "The Solution",
        description:
          "Flavour of the youth campaign- Announcing what the brand stands for, making everyday moments everyday moment. You can have fun moment at everytime. Youth is a mindset. Smoov Chapman is a flavour of youth. We did a TVC for this, it was a 360 campaign. The song used in the TVC and used it for a digital campaign, engaging audience in a fun way by telling them to dance to it. We use 2 dance group for influencer campaign and used one dancer as a dance judge, so we used epeople that are key and known to other people.",
        images: [],
      },
    ],
    result: [],
  },
  {
    createdOn: new Date(2021, 7, 8).toLocaleString(),
    // color: "#00AEED",
    slug: "the-month-of-jollof",
    title: "The Month of Jollof",
    clients: "Maggi",
    industries: "Food",
    // description: "Fun is an attitude",
    banner: "/images/case-study/maggi/the-month-of-jollof-1.jpg",
    content: [
      {
        title: "The Task",
        description:
          "To many Nigerians, Jollof is more than a meal. It's a national treasure. August 22nd is recognised as world Jollof day and in line with Maggi's goal to become number one food brand, the task was to make it a month long celebration.",
        images: [
          "/images/case-study/maggi/the-month-of-jollof-1.jpg",
          "/images/case-study/maggi/the-month-of-jollof-2.jpg",
        ],
      },
      {
        title: "The Idea",
        description:
          "We created a bank of content that cut across memes, versus battles between types of jollof and “witty” quotes to drive resonance and engagement.  We also leveraged influencers to further amplify our content pieces.",
        images: [
          "/images/case-study/maggi/the-month-of-jollof-3.jpg",
          "/images/case-study/maggi/the-month-of-jollof-4.jpg",
        ],
      },
    ],
    result: [],
  },
  {
    createdOn: new Date(2021, 7, 8).toLocaleString(),
    // color: "#00AEED",
    slug: "food-is-the-way-to-everyones's-heart",
    title: "Food is the way to everyones's heart",
    clients: "Maggi",
    // description: "Fun is an attitude",
    industries: "Food",
    banner:
      "/images/case-study/maggi/food-is-the-way-to-everyones's-heart-0.jpg",
    content: [
      {
        title: "The Task",
        description:
          "Create an engaging and compelling digital campaign for Valentine's day for the brand Maggi.",
        images: [
          "/images/case-study/maggi/food-is-the-way-to-everyones's-heart-1.jpg",
          "/images/case-study/maggi/food-is-the-way-to-everyones's-heart-2.jpg",
        ],
      },
      {
        title: "The Idea",
        description:
          "We created an inclusive campaign that celebrated different kinds of love i.e the love between friends, older couples, family members and most importantly, the love between humans and food. Our campaign also highlighted the place of food in these different types of relationships and tapped into a cultural phrase (food is the way to a man's heart) to pass a stronger message across. By showing that food is the way to everyone's heart we were able to show why cooking shouldn't be reserved for one gender.",
        images: [
          "/images/case-study/maggi/food-is-the-way-to-everyones's-heart-3.jpg",
          "/images/case-study/maggi/food-is-the-way-to-everyones's-heart-4.jpg",
        ],
      },
    ],
    result: [],
  },
  {
    createdOn: new Date(2021, 7, 8).toLocaleString(),
    color: "#00AEED",
    slug: "everything-will-rise-again",
    title: "Everything Will Rise Again",
    clients: "Union Bank",
    industries: "Banking",
    description: "your simpler, smarter bank",
    banner: "/images/case-study/union-bank/everything-will-rise-again-0.jpg",
    content: [
      {
        title: "The Situation",
        description:
          "Due to the Coronavirus pandemic, Nigeria joins other countries in the world to impose a lockdown. Putting businesses, families and individuals in a tight place. Easter approached but there seemed to be no hope in sight.",
        images: [],
      },
      {
        title: "The Task",
        description:
          "Create a thematic message for Easter to ignite hope and optimism in Nigerians who were going through one of the most difficult times of their lives.",
        images: [
          "/images/case-study/union-bank/everything-will-rise-again-1.jpg",
          "/images/case-study/union-bank/everything-will-rise-again-2.jpg",
        ],
      },
      {
        title: "The Idea",
        description:
          "We brought the very essence  of the season - resurrection - to all the aspects of life that has been shut down and gave everyone the hope to apply it to every area of their lives that may have been shut down.",
        images: [
          "/images/case-study/union-bank/everything-will-rise-again-3.jpg",
          //   "/images/case-study/union-bank/everything-will-rise-again-4.jpg",
        ],
      },
    ],
    result: [],
  },
  {
    createdOn: new Date(2021, 7, 8).toLocaleString(),
    color: "#00AEED",
    slug: "the-rise-challenge",
    title: "Everything Will Rise Again",
    clients: "Union Bank",
    industries: "Banking",
    description: "your simpler, smarter bank",
    banner: "/images/case-study/union-bank/the-rise-challenge-0.png",
    content: [
      {
        title: "The Task",
        description:
          "Design a social intervention campaign to sustain hope and optimism amongst everyday Nigerians who could not bank on planned govt intervention to survive the extended lockdown",
        images: [
          "/images/case-study/union-bank/the-rise-challenge-1.jpg",
          "/images/case-study/union-bank/the-rise-challenge-2.jpg",
        ],
      },
      {
        title: "The Idea",
        description:
          "Challenge Nigerians to share their stories of how they were rising above the pandemic to create ripple effects of that same action across the entire nation.",
        images: [
          "/images/case-study/union-bank/the-rise-challenge-3.jpg",
          "/images/case-study/union-bank/the-rise-challenge-4.jpg",
        ],
      },
    ],
    result: [
      {
        title: null,
        units: null,
        description:
          "The UnionRiseChallenge created a ripple effect of hope and optimism and shifted the conversation from what the virus was doing to the inspiring stories of how individuals, families and small businesses were rising above the challenge.",
        image: null,
      },
    ],
  },
  {
    createdOn: new Date(2021, 7, 8).toLocaleString(),
    // color: "#00AEED",
    slug: "the-red-shopper-promo",
    title: "The Red Shopper Promo",
    clients: "Nestle",
    // description: "your simpler, smarter bank",
    industries: "Food",
    banner: "/images/case-study/nestle/the-red-shopper-promo-0.jpg",
    content: [
      {
        title: "The Task",
        description:
          "Maintain excitement for the 3rd edition of the Get Started Shopper Promotion.",
        images: [
          "/images/case-study/nestle/the-red-shopper-promo-1.jpg",
          "/images/case-study/nestle/the-red-shopper-promo-2.jpg",
        ],
      },
      {
        title: "The Idea",
        description:
          "We conducted a 360 campaign that ran across digitial and traditional channels. In order to create a more meaningful campaign we tapped into the hustle spirit in our TG who are Milennials and Gen Z's.",
        images: [
          "/images/case-study/nestle/the-red-shopper-promo-3.jpg",
          "/images/case-study/nestle/the-red-shopper-promo-4.jpg",
        ],
      },
    ],
    result: [],
  },
  {
    createdOn: new Date(2021, 7, 8).toLocaleString(),
    // color: "#00AEED",
    slug: "5-alive",
    title: "5Alive",
    clients: "5Alive",
    industries: "Drinks",
    description: "made with nature",
    banner: "/images/case-study/5-alive/5-alive-0.png",
    content: [
      {
        title: "The Brief",
        description:
          "Monthly digital content calendar for the brand's social media platforms.",
        images: [
          "/images/case-study/5-alive/5-alive-1.png",
          "/images/case-study/5-alive/5-alive-2.png",
        ],
      },
    ],
    result: [],
  },
  {
    createdOn: new Date(2021, 7, 8).toLocaleString(),
    // color: "#00AEED",
    slug: "endeavor-nigeria",
    title: "Endeavor Nigeria",
    clients: "Endeavor Nigeria",
    // description: "made with nature",
    industries: "Banking",
    banner: null,
    content: [
      {
        title: "The Brief",
        description:
          "Develop content & design templates for Endeavor's social media platforms and email newsletters.",
        images: [
          "/images/case-study/endeavor-nigeria/endeavor-nigeria-1.png",
          "/images/case-study/endeavor-nigeria/endeavor-nigeria-2.jpg",
        ],
      },
    ],
    result: [],
  },
  {
    createdOn: new Date(2021, 7, 8).toLocaleString(),
    // color: "#00AEED",
    slug: "absolut-vodka",
    title: "Absolut Vodka",
    clients: "Absolut Vodka",
    // client: "Endeavor Nigeria",
    // description: "made with nature",
    industries: "Drinks",
    banner: null,
    content: [
      {
        title: "The Brief",
        description:
          "Monthly digital content calendar for the brand's social media platforms",
        images: [
          "/images/case-study/absolut-vodka/absolut-vodka-1.jpg",
          "/images/case-study/absolut-vodka/absolut-vodka-2.jpg",
        ],
      },
    ],
    result: [],
  },
  {
    createdOn: new Date(2021, 7, 8).toLocaleString(),
    // color: "#00AEED",
    slug: "bold-bitter-lemon",
    title: "Bold Bitter Lemon",
    clients: "Bold",
    // description: "made with nature",
    industries: "Drinks",
    banner: "/images/case-study/bold/bold-bitter-lemon-0.png",
    content: [
      {
        title: "The Brief",
        description:
          "Monthly digital content calendar for the brand's social media platforms",
        images: [
          "/images/case-study/bold/bold-bitter-lemon-1.png",
          "/images/case-study/bold/bold-bitter-lemon-2.gif",
        ],
      },
    ],
    result: [],
  },
];
